const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let usuario = {
    id: '',
    nombre: '',
    apellido: '',
    fecha_creacion: ''
};

let usuarios = [];

let respuesta = {
    error: false,
    codigo: 200,
    mensaje: ''
}

app.get('/', function (req, res) {
    respuesta = {
        error: true,
        codigo: 200,
        mensaje: 'Punto de inicio'
    };

    res.send(respuesta);
});

//Consultar usuario
app.get('/usuario', function (req, res) {
    if (req.query.id === '' || req.query.id == undefined) {
        respuesta = {
            error: true,
            codigo: 501,
            mensaje: 'El id es requerido.'
        };
    } else if (usuarios.find(x => x.id === req.query.id)) {
        respuesta = {
            error: false,
            codigo: 200,
            mensaje: 'El usuario existe.',
            respuesta: usuarios.find(x => x.id === req.query.id)
        };
    } else {
        respuesta = {
            error: true,
            codigo: 501,
            mensaje: 'El usuario no existe.'
        };
    }

    res.send(respuesta);
    console.log(usuarios);
});

//Crear usuario
app.post('/usuario', function (req, res) {
    if (usuarios.find(x => x.id === req.query.id)) {
        respuesta = {
            error: true,
            codigo: 503,
            mensaje: 'El usuario con el id ' + req.query.id + ' ya existe.',
            respuesta: usuarios.find(x => x.id === req.query.id)
        };
    } else if (!req.query.id || !req.query.nombre || !req.query.apellido) {
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'El id, nombre y apellido son requeridos.'
        };
    } else {

        usuario = {
            id: req.query.id,
            nombre: req.query.nombre,
            apellido: req.query.apellido,            
            fecha_creacion: new Date()
        }

        usuarios.push(usuario);

        respuesta = {
            error: false,
            codigo: 200,
            mensaje: 'Usuario creado.',
            respuesta: usuario
        };
    }

    res.send(respuesta);
    console.log(usuarios);
});

//Actualizar usuario
app.put("/usuario", function (req, res) {
    if (!req.query.id){
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'El id es requerido.'
        };
    } else if (!usuarios.find(x => x.id === req.query.id)) {
        respuesta = {
            error: true,
            codigo: 501,
            mensaje: 'El usuario no existe.'
        };
    } else {
        let index = usuarios.findIndex(x => x.id === req.query.id)

        usuarios[index].nombre = req.query.nombre;
        usuarios[index].apellido = req.query.apellido;        

        respuesta = {
            error: false,
            codigo: 200,
            mensaje: 'El usuario ha sido actualizado.',
            respuesta: usuario
        };
    }

    res.send(respuesta);
    console.log(usuarios);
});

//Eliminar usuario
app.delete('/usuario', function (req, res) {
    if (!usuarios.find(x => x.id === req.query.id)) {
        respuesta = {
            error: true,
            codigo: 501,
            mensaje: 'El usuario no existe.'
        };
    } else {
        
        usuarios.splice(usuarios.indexOf(x => x.id === req.query.id),1);

        respuesta = {
            error: false,
            codigo: 200,
            mensaje: 'El usuario con el id ' + req.query.id + ' ha sido eliminado.'
        };
    }

    res.send(respuesta);
    console.log(usuarios);
});

//Cuando no se encuentra la ruta
app.use(function (req, res, next) {
    respuesta = {
        error: true,
        codigo: 404,
        mensaje: 'URL no encontrada.'
    }

    res.status(404).send(respuesta);
})

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

